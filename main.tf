provider "helm" {
  kubernetes {
    config_context         = "k3d-k8s"
    config_context_cluster = "k3d-k8s"
    config_path            = "~/.kube/config"
  }
}

locals {
  target_revision = "HEAD"
  base_domain     = "project.local"
  repo_url        = "https://gitlab.com/project-gitops/argocd.git"
}

resource "helm_release" "argocd" {
  name              = "argocd"
  chart             = "${path.module}/ArgoCD/tools/argocd"
  namespace         = "argocd"
  dependency_update = true
  create_namespace  = true
  timeout           = 1750

  values = [
    file("./ArgoCD/tools/argocd/values.yaml"),
    <<EOT
    argo-cd:
      server:
        config:
            repositories: |
              - url: ${local.repo_url}
    EOT
  ]
}

resource "helm_release" "services" {
  depends_on        = [helm_release.argocd]
  name              = "services"
  chart             = "${path.module}/ArgoCD/tools/services"
  namespace         = "argocd"
  dependency_update = true
  create_namespace  = true

  values = concat([
    templatefile("./ArgoCD/tools/services/values.tmpl.yaml",
      {
        target_revision = local.target_revision
        repo_url        = local.repo_url
        cluster_name    = "k3d-k8s"
        base_domain     = local.base_domain
        extra_apps      = var.extra_apps
      }
    )],
    var.app_of_apps_values_overrides,
  )
}

variable "app_of_apps_values_overrides" {
  description = "Extra value files content for the App of Apps"
  type        = list(string)
  default     = []
}

variable "extra_apps" {
  description = "Extra applications to deploy."
  type        = list(any)
  default     = []
}