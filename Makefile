.PHONY: install create argocd delete

#https://github.com/rancher/k3d#get
install:
	wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | TAG=v4.1.1 bash

create:
	k3d cluster create k8s \
	--servers 1 --agents 1 \
	--api-port 127.0.0.1:6443 -p 80:80@loadbalancer -p 443:443@loadbalancer \
	--k3s-server-arg "--no-deploy=traefik" --kubeconfig-switch-context

	kubectl cluster-info

argocd:
	terraform init && terraform fmt && terraform validate && \
	terraform plan -out=config  && terraform apply config


delete:
	k3d cluster delete -a && \
	rm -rf .terraform* config terraform.tfstate* crash.log
