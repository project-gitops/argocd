# Project #01

## Install

```sh 
# Install k3d
$ make install

# Creater cluster k3d
$ make create

# Install ArgoCD
$ make argocd

# Delete cluster k3d
$ make delete
```
